return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 300

  local wk = require("which-key")

  wk.register({
      ["<leader>f"] = { name = " Find Files" },
      ["<leader>t"] = { name = " Tabs" },
      ["<leader>e"] = { name = " File Explorer" },
      ["<leader>c"] = { name = " Code Actions" },
      ["<leader>n"] = { name = " Clear highlights" }
    })
  end,
  opts = {

  },

}
