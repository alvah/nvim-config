return {
    'goolord/alpha-nvim',
    event = "VimEnter",
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    config = function ()
        local alpha = require'alpha'
        local startify = require'alpha.themes.dashboard'
        startify.section.header.val = {
            [[                                                                                                   ]],
            [[                                                                                                   ]],
            [[                                                                                                   ]],
            [[                                                                                                   ]],
            [[                                                                                                   ]],
            [[                                                                                                   ]],
            [[ /\\\\\_____/\\\_______________________________/\\\________/\\\___________________________         ]],
            [[ \/\\\\\\___\/\\\______________________________\/\\\_______\/\\\__________________________         ]],
            [[ _\/\\\/\\\__\/\\\______________________________\//\\\______/\\\___/\\\_____________________       ]],
            [[  _\/\\\//\\\_\/\\\_____/\\\\\\\\______/\\\\\_____\//\\\____/\\\___\///_____/\\\\\__/\\\\\__       ]],
            [[   _\/\\\\//\\\\/\\\___/\\\/////\\\___/\\\///\\\____\//\\\__/\\\_____/\\\__/\\\///\\\\\///\\\_     ]],
            [[    _\/\\\_\//\\\/\\\__/\\\\\\\\\\\___/\\\__\//\\\____\//\\\/\\\_____\/\\\_\/\\\_\//\\\__\/\\\     ]],
            [[     _\/\\\__\//\\\\\\_\//\\///////___\//\\\__/\\\______\//\\\\\______\/\\\_\/\\\__\/\\\__\/\\\_   ]],
            [[      _\/\\\___\//\\\\\__\//\\\\\\\\\\__\///\\\\\/________\//\\\_______\/\\\_\/\\\__\/\\\__\/\\\   ]],
            [[       _\///_____\/////____\//////////_____\/////___________\///________\///__\///___\///___\///__ ]],
            [[                                                                                                   ]],
        }
        startify.section.buttons.val = {
            startify.button('n', '  New file', ':ene <BAR> startinsert <CR>'),
            startify.button('r', '  Recent files', ":Telescope oldfiles<CR>"),
            startify.button('f', '  Find file', ':Telescope find_files<CR>'),
            startify.button('h', '  Load session', ':source Session.vim <CR>'),
            startify.button('u', '  Update plugins', ':Lazy update <CR>'),
            startify.button('m', '  Mason', ':Mason <CR>'),
            startify.button('q', '󰗼  Quit', ':qa! <CR>'),
        }
        alpha.setup(startify.config)
    end
}


